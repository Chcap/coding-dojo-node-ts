# Template projet Coding Dojo

Ceci est un template de projet pour coding dojo, utilisant:
- nodejs
- jest
- typescript

## How to

- Lancement des tests avec `npm test`
- Lancement des tests en mode auto avec `npm run tw`
