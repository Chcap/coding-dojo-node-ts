import { demo } from '../src/demo'

describe('Demo', () => {

    test('should do the sum', () => {
        const result = demo(1, 2)

        expect(result).toEqual(3)
    })
})
